# Authors

Currently maintained by **Philipp Zschumme** and **Nico Brandt**.

List of contributions from the Kadi4Mat team and other contributors, ordered by
date of first contribution:

* **Philipp Zschumme**
* **Nico Brandt**
* **Ephraim Schoof**
* **Patrick Altschuh**
* **Raphael Schoof**
* **Lars Griem**
* **Christoph Herrmann**
* **Zihan Zhang**
* **Julian Grolig**
* **Arnd Koeppe**
* **Kai Sellschopp**
