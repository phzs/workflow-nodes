Upgrading
=========

To upgrade the library, the ``-U`` flag can be used:

.. code-block:: shell

    $ pip3 install workflow-nodes -U

When installing from source for development, fetch the latest code first and install it
in editable mode again:

.. code-block:: shell

    $ pip3 install -e .[dev]
